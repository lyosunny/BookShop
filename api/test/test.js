const api = require('../services/api');
const chai = require('chai')
const chaiHttp = require('chai-http')
const expect = chai.expect
chai.use(chaiHttp);

const domain = 'https://dry-tundra-25227.herokuapp.com'
const userid = '5aba6864f52c940014c9731e'

describe('API Unit Test', () => {
    describe('/createUser', () => {
        it('should create a user and return 201', done => {
            chai.request(domain).post('/createUser')
            .send({
                name: 'test01',
                email: 'test01@a.com',
                password: '111111',
            }).end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(201)
                done()
            })
        })
    })

    describe('deleteUser', () => {
        it('should create a user and return 204', done => {
            chai.request(domain).del(`/deleteUser/${userid}`)
            .end((err, res) => {
                expect(err).to.be.null
                expect(res).to.have.status(204)
                done()
            })
        })
    })

    describe('/getAllUser', () => {
        it('should match the length' , done => {
            chai.request(domain).get('/getAllUser').end((err, res) => {
                expect(err).to.be.null
                expect(res.body).to.have.length(1)
                done()
            })
        })
    })

    describe('/getUser', () => {
        it('should match test01, test' , done => {
            chai.request(domain).get(`/getUser/${userid}`).end((err, res) => {
                expect(err).to.be.null
                expect(res.body.name).to.equal('test01');
                done()
            })
        })    
    })

    describe('/updateBookmark', () => {
        it('should add bookmark to user', done => {
            api.searchBooksVolume("javascript", (json) => {
                chai.request(domain).put(`/updateBookmark/${userid}`)
                .send({ bookid:json.items[1].id, markName:'jsbook', page:10 })
                .end((err,res) => {
                    expect(err).to.be.null
                    expect(res).to.have.status(200)
                    done()     
                })
            })            
        })
    })

    describe('/deleteBookmark', () => {
        it('should delete bookmark to user', done => {
            chai.request(domain).put(`/deleteBookmark/${userid}`)
            .send({ bookid:'' })
            .end((err,res) => {
                expect(err).to.be.null
                expect(res).to.have.status(200)
                done()     
            })            
        })
    })
})