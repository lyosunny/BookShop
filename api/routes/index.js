/**
 * Module Dependencies
 */
const errors = require('restify-errors');
/**
 * Model Schema
 */
const User = require('../models/user');

module.exports = function(server) {

	/**
	 * Create User
	 */
	server.post('/createUser', (req, res, next) => {
		if (!req.is('application/json')) {
			return next(
				new errors.InvalidContentError("Expects 'application/json'"),
			);
		}

		let data = req.body || {};

		let user = new User(data);
		user.save(err => {
			if (err) {
				console.error(err);
				return next(new errors.InternalError(err.message));
				next();
			}

			res.send(201);
			next();
		});
	});

	/**
	 *  Redirect
	 */
	server.get('/', (req, res, next) => {
		res.redirect('/getAllUser',next)
	})

	/**
	 * LIST ALL USER
	 */
	server.get('/getAllUser', (req, res, next) => {
		User.apiQuery(req.params, (err, docs) => {
			if (err) {
				console.error(err);
				return next(
					new errors.InvalidContentError(err.errors.name.message),
				);
			}

			res.send(docs);
			next();
		});
	});

	/**
	 * GET User By ID
	 */
	server.get('/getUser/:user_id', (req, res, next) => {
		User.findById(req.params.user_id , (err, doc) => {
			if (err) {
				console.error(err);
				return next(
					new errors.InvalidContentError(err.errors.name.message),
				);
			}

			res.send(doc);
			next();
		});
	});

	/**
	 * Update User Bookmark
	 */
	server.put('/updateBookmark/:user_id', (req, res, next) => {
		if (!req.is('application/json')) {
			return next(
				new errors.InvalidContentError("Expects 'application/json'"),
			);
		}

		const data = {
			bookid: req.body.bookid, 
			markName: req.body.markName, 
			page: req.body.page			
		}

		User.findByIdAndUpdate(req.params.user_id, 
			{ $push: { bookmarks: data } },
			(err, doc) => {
				if (err) {
					console.error(err);
					return next(
						new errors.InvalidContentError(err.message),
					);
				} else if (!doc) {
					return next(
						new errors.ResourceNotFoundError(
							'The resource you requested could not be found.',
						),
					);
				}

				res.send(200, data);
				next();
			}
		)
	})

	/**
	 * Remove Bookmark
	 */
	server.put('/deleteBookmark/:user_id', (req, res, next) => {
		if (!req.is('application/json')) {
			return next(
				new errors.InvalidContentError("Expects 'application/json'"),
			);
		}

		const data = {
			bookid: req.body.bookid
		}

		User.findByIdAndUpdate(req.params.user_id,
			{ $pull: { bookmarks: data} },
			(err, doc) => {
				if (err) {
					console.error(err);
					return next(
						new errors.InvalidContentError(err.errors.name.message),
					);
				} else if (!doc) {
					return next(
						new errors.ResourceNotFoundError(
							'The resource you requested could not be found.',
						),
					);
				}

				res.send(200, data);
				next();
			}
		)
	});

	/**
	 * UPDATE UserInfo
	 */
	server.put('/updateUserInfo/:user_id', (req, res, next) => {
		if (!req.is('application/json')) {
			return next(
				new errors.InvalidContentError("Expects 'application/json'"),
			);
		}

		let data = req.body || {};

		if (!data._id) {
			data = Object.assign({}, data, { _id: req.params.user_id });
		}

		
		User.findOne({ _id: req.params.user_id }, (err, doc) => {
			if (err) {
				console.error(err);
				return next(
					new errors.InvalidContentError(err.errors.name.message),
				);
			} else if (!doc) {
				return next(
					new errors.ResourceNotFoundError(
						'The resource you requested could not be found.',
					),
				);
			}

			User.update({ _id: data._id }, data, { runValidators: true }, err => {
				if (err) {
					console.error(err);
					return next(
						new errors.InvalidContentError(err.message),
					);
				}

				res.send(200, data);
				next();
			});
		});
	});

	/**
	 * DELETE User By ID
	 */
	server.del('/deleteUser/:user_id', (req, res, next) => {
		User.remove({ _id: req.params.user_id }, function(err) {
			if (err) {
				console.error(err);
				return next(
					new errors.InvalidContentError(err.errors.name.message),
				);
			}

			res.send(204);
			next();
		});
	});
};