const fetch = require('node-fetch');
const { URL } = require('url');
const urlStr = 'https://www.googleapis.com/books/v1/volumes';

/**
 * fetch function 
 */
module.exports = {

	//search book by key word
	searchBooksVolume : function(query, callBack){
		if(!query){
			return false;
		}
		const url = new URL(urlStr);
		const params = { q:query }

		Object.keys(params).forEach(key => url.searchParams.append(key, params[key]))

		fetch(url).then(res => { 
			if(res.ok) return res.json()
			else return false
		}).then(json => {
			callBack(json)
		})
	},

	//get book detail by volumeId
	getBooksVolumeById: function(id, callBack){
		if(!id){
			return false;
		}
		const url = new URL(`${urlStr}/${id}`);
	
		fetch(url).then(res => { 
			if(res.ok) return res.json()
			else return false
		}).then(json => {
			callBack(json)
		})
	}
}