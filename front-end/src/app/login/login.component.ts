import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoggedIn = false;
  name = '';
  email = '';
  password = '';
  signUpName = '';
  signUpEmail = '';
  signUpPw = '';
  show = false;

  constructor(private http:HttpClient) { }

  ngOnInit() {
    let user = JSON.parse(sessionStorage.getItem('user'));
    if(user != null){
      this.isLoggedIn = true;
      this.name = user['name'];
    }
  }

  login() {
    this.http.get('https://dry-tundra-25227.herokuapp.com/getAllUser')
    .subscribe((res:Array<Object>) => {
      let user = res.find(value => this.email == value['email'] && this.password == value['password']);
      if(user){
        this.isLoggedIn = true;
        this.name = user['name'];
        sessionStorage.setItem('user',JSON.stringify(user));
      }else {
        alert('User not find.');
      }
      
    }, err => alert(err));
  }

  logout(){
    this.isLoggedIn = false;
    sessionStorage.removeItem('user');
  }

  signUp(){
    this.http.post('https://dry-tundra-25227.herokuapp.com/createUser',{
      name:this.signUpName,
      email:this.signUpEmail,
      password:this.signUpPw
    }).subscribe(next => alert("Success"), err => alert("Error"));
  }
}


