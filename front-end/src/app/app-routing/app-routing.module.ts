import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookListComponent } from '../book-list/book-list.component';
import { LoginComponent } from '../login/login.component';
import { PageNotFoundComponent } from '../not-found.component';
import { MyBookmarksComponent } from '../my-bookmarks/my-bookmarks.component';
import { BookDetailsComponent } from '../book-details/book-details.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'booklist', component: BookListComponent },
  { path: 'mybookmarks', component: MyBookmarksComponent },
  { path: 'bookDetail', component: BookDetailsComponent },
  { path: '', redirectTo:'/booklist', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];  

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
