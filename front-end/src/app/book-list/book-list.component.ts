import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  items;
  query;

  constructor(private http:HttpClient, private router:Router) { }

  ngOnInit() {
    if(!this.items){
      this.http.get('https://www.googleapis.com/books/v1/volumes',
      {
        params:{
          q:'javascript',
          filter: 'ebooks',
          fields: 'items(id,selfLink,volumeInfo)'
        }
      }).subscribe(next => this.items = next['items']);
    }
  }

  search(){
    this.http.get('https://www.googleapis.com/books/v1/volumes',
      {
        params:{
          q:this.query,
          filter: 'ebooks',
          fields: 'items(id,selfLink,volumeInfo)'
        }
      }).subscribe(next => this.items = next['items']);
  }

  detail(id,link){
    this.router.navigate(['/bookDetail',{id:id, link:link} ])
  }
}
