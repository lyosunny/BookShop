import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpClient } from '@angular/common/http';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  src;
  book;
  sub;
  isLoggedin = false;

  constructor(private route: ActivatedRoute, public sanitizer: DomSanitizer, private http:HttpClient) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      let param = params;
      this.src = `https://books.google.com.hk/books?id=${param.id}&lpg=PP1&dq=javascript&as_brr=5&hl=zh-TW&pg=PP1&output=embed`;
      this.http.get(param.link).subscribe(n => this.book = n);
    });
    let user = JSON.parse(sessionStorage.getItem('user'));
    this.isLoggedin = user != null
  }

  ngOnDestory(){
    this.sub.unsubscribe();
  }

  add(){
    let user = JSON.parse(sessionStorage.getItem('user'));
    this.http.put(`https://dry-tundra-25227.herokuapp.com/updateBookmark/${user._id}`, { "bookid": this.book.id } ).subscribe(n => {
      console.log(n);
    }, err => console.log(err));
  }

}
