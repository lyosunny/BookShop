import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-bookmarks',
  templateUrl: './my-bookmarks.component.html',
  styleUrls: ['./my-bookmarks.component.css']
})
export class MyBookmarksComponent implements OnInit {

  isLoggedin = false;
  bookmarks = [];

  constructor(private http:HttpClient, private router:Router) { }

  ngOnInit() {
    let user = JSON.parse(sessionStorage.getItem('user'));
    if(user != null){
      this.isLoggedin = true;
      this.http.get(`https://dry-tundra-25227.herokuapp.com/getUser/${user._id}`).subscribe(n => {
        n['bookmarks'].forEach(element => {
          this.http.get(`https://www.googleapis.com/books/v1/volumes/${element['bookid']}`, { params: { fields : 'id,selfLink,volumeInfo'} }).subscribe(next => this.bookmarks.push(next));
        });
      });        
    }
  }

  detail(id,link){
    this.router.navigate(['/bookDetail',{id:id, link:link} ])
  }

  remove(id){
    let user = JSON.parse(sessionStorage.getItem('user'));
    this.http.put(`https://dry-tundra-25227.herokuapp.com/deleteBookmark/${user._id}`, { "bookid": id }).subscribe(n => {
      console.log(n);
      let index = this.bookmarks.findIndex(v => id == v._id);
      this.bookmarks.splice(index,1);
    }, err => console.log(err));
  }
}
